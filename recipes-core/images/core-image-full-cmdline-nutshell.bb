DESCRIPTION = "A console-only image with more full-featured Linux system \
functionality installed."

IMAGE_FEATURES += "splash ssh-server-openssh"
VIRTUAL-RUNTIME_init_manager = "systemd"

IMAGE_INSTALL = "\
    packagegroup-core-boot \
    packagegroup-core-full-cmdline \
    ${CORE_IMAGE_EXTRA_INSTALL} \
    "

inherit core-image
inherit systemd

SYSTEMD_PACKAGES ?= "${PN}"

SYSTEMD_SERVICE = "first.service"	

SYSTEMD_AUTO_ENABLE  ??= "enable"


