require recipes-kernel/linux/linux-mainline-common.inc

LICENSE = "CLOSED" 

SRC_URI = "git://github.com/OSCPU/riscv-linux.git;protocol=https;branch=zynq-standalone \
           file://001-fix-yylloc-variable.patch \
           "
SRCREV = "c4490f2dabc1219140390fc78ed1a2f805fc0017"

PV = "1.0+git${SRCPV}"

S = "${WORKDIR}/git"

PREFERRED_PROVIDER_virtual/kernel ?= "linux-nutshell"

KBUILD_DEFCONFIG = "zynq_standalone_defconfig"
 
COMPATIBLE_MACHINE = "nutshell|qemux86|qemux86-64"

KERNEL_IMAGETYPE = "vmlinux"
KERNEL_FEATURES:remove = "cfg/fs/vfat.scc" 
KERNEL_VERSION_SANITY_SKIP="1"

do_kernel_configcheck() {
}

do_kernel_link_images() {
}


do_compile:append() {
   make ARCH=riscv zynq_standalone_defconfig
  
}
do_deploy(){
    install -m 0644 "${B}/vmlinux" ${D}/vmlinux
}

do_install() {
   install -m 0644 "${B}/vmlinux" ${S}/vmlinux

   install -m 0755 -d ${DEPLOY_DIR_IMAGE}/
   install -m 0644 ${S}/vmlinux ${DEPLOY_DIR_IMAGE}/
}
