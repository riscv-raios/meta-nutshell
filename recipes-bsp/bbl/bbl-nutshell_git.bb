LICENSE = "CLOSED"

MACHINE = "nutshell"

COMPATIBLE_MACHINE = "nutshell"

SRC_URI = "git://github.com/OSCPU/riscv-pk.git;protocol=https;branch=zynq-standalone \
          file://riscv_pk.patch \ 
          "
# Modify these as desired
PV = "1.0+git${SRCPV}"
SRCREV = "bbeb270f1f750e893f314d29f745d988fe218542"
S = "${WORKDIR}/git"

EXTRA_OEMAKE:class-target = 'CROSS_COMPILE="${TARGET_PREFIX}" CC="${CC} ${CFLAGS} ${LDFLAGS}" HOSTCC="${BUILD_CC} ${BUILD_CFLAGS} ${BUILD_LDFLAGS}" STRIP=true V=1'
EXTRA_OEMAKE:class-native = 'CC="${BUILD_CC} ${BUILD_CFLAGS} ${BUILD_LDFLAGS}" HOSTCC="${BUILD_CC} ${BUILD_CFLAGS} ${BUILD_LDFLAGS}" STRIP=true V=1'
EXTRA_OEMAKE:class-nativesdk = 'CROSS_COMPILE="${HOST_PREFIX}" CC="${CC} ${CFLAGS} ${LDFLAGS}" HOSTCC="${BUILD_CC} ${BUILD_CFLAGS} ${BUILD_LDFLAGS}" STRIP=true V=1'

PREFERRED_PROVIDER_virtual/bootloader = "bbl-nutshell"
 
OECORE_NATIVE_SYSROOT ="${WORKDIR}/recipe-sysroot-native/"
KERNEL_FEATURES:remove = "cfg/fs/vfat.scc"
KERNEL_VERSION_SANITY_SKIP="1"
INHIBIT_SYSROOT_STRIP = "1"

do_patch() {
	cd ${S}
	patch -p1 < ${WORKDIR}/riscv_pk.patch
}

do_configure() {
	tempPath=${WORKDIR}/build
	if [! -d "$tempPath" ]; then
	mkdir $tempPath
	fi
}

do_compile() {
	cp ${WORKDIR}/recipe-sysroot/usr/include/gnu/stubs-lp64d.h ${WORKDIR}/recipe-sysroot/usr/include/gnu/stubs-lp64.h
	tempPath=${WORKDIR}/riscv-linux
	if [ -d "$tempPath" ]; then
	rm -rf $tempPath
	fi
	cp -r ${TOPDIR}/tmp-glibc/work-shared/nutshell/kernel-source/. ${WORKDIR}/riscv-linux/ 
	oe_runmake
}

do_deploy:append() {
	install -m 0755 ${S}/build/bbl.bin   ${D}/bbl.bin
}

do_install:append() {
	install -m 0755 -d ${DEPLOY_DIR_IMAGE}/
	install -m 0755 ${S}/build/bbl.bin ${DEPLOY_DIR_IMAGE}/
}


BBCLASSEXTEND = "native nativesdk"
INSANE_SKIP_nativesdk-qtbase = "installed-vs-shipped"
FILES_${PN} += "${libdir}/*"
FILES_${PN}-dev = "${libdir}/* ${includedir}"
